<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'campaigns';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['brand_id', 'title', 'description', 'photo', 'starts_on', 'ends_on', 'status'];

    protected $appends = ['matched', 'claimed', 'elipsis', "added_on"];

    public function scopeSearch($query, $string)
    {
        if ($string) {
            $columns = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());
            foreach ($columns as $column) {
                $query->orWhere($column, 'LIKE', $string . '%');
                //$query->orWhere($column, 'LIKE', '%' . $string . '%');
            }
            return $query;
        }
        return $query;

    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
    public function inbound_inventories()
    {
        return $this->hasOne('App\InboundInventory');
    }

    public function survey()
    {
        return $this->hasOne('App\FeedbackSurvey');
    }

    public function page()
    {
        return $this->hasOne('App\CampaignPage');
    }

    // public function matching_tags()
    // {
    //     return $this->belongsToMany('App\MatchingTag', 'campaign_matching_tags')->withPivot('value');
    // }

    public function matching_data()
    {
        return $this->hasMany('App\CampaignMatchingData');
    }

    public function feedbacks()
    {
        return $this->hasMany('App\FeedbackSurvey');
    }

    public function categories()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function consumers()
    {
        return $this->belongsTo('App\ConsumerCampaign');
    }

    public function getPhotoAttribute($value)
    {
        $photos = $value;
        return asset(\Illuminate\Support\Facades\Storage::url($photos));
    }

    public function getStartsOnAttribute($value)
    {
        return Carbon::parse($value)->format('D jS \\of M Y h:i:s A');
    }

    public function getEndsOnAttribute($value)
    {
        return Carbon::parse($value)->format('D jS \\of M Y h:i:s A');
    }

    public function getAddedOnAttribute(){
        return Carbon::parse($this->created_at)->format('D jS \\of M Y h:i:s A');
    }


    public function getMatchedAttribute()
    {
        $campaign_matching_data = CampaignMatchingData::where('campaign_id', $this->id)->get();;
        $get = ConsumerMatchingData::whereIn('matching_data_id', $campaign_matching_data->pluck('matching_data_id'))->get();
        return Consumer::whereIn('id', $get->pluck('consumer_id'))->count();
    }

    public function getClaimedAttribute()
    {
        $claimed = $this->inbound_inventories()->sum('claimed');
        return intval($claimed);
    }

    public function getElipsisAttribute(){
        return Str::limit($this->title, '33');
    }

}
