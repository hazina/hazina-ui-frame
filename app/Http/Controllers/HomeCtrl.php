<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\User;
use Illuminate\Http\Request;

class HomeCtrl extends Controller
{
    public function home(Request $request){

       return view('welcome', []);
    }

    public function campaign_landing(Request $request, $id){
        $campaign = Campaign::with(['page', 'brand'])->where('id', $id)->first();
       return view('campaign.landing', ['campaign'=> $campaign]);
    }
}
