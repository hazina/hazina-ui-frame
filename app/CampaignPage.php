<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignPage extends Model
{

    protected $fillable = ['data'];

    public function campaign(){
        return $this->belongsTo('App\Campaign');
    }
}
