<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('portal/consumer')->namespace('Pwa')->middleware([])->group(function () {
//     Route::get('/{any?}', 'ConsumerCtrl@home')->name('consumer.home')->where('any', '.*');
// });

Route::prefix('portal/brands')->namespace('Pwa')->middleware([])->group(function () {
    Route::get('/{any?}', 'BrandCtrl@home')->name('brand.home')->where('any', '.*');
});

Route::prefix('portal/admin')->namespace('Pwa')->middleware([])->group(function () {
    Route::get('/{any?}', 'AdminCtrl@home')->name('admin.home')->where('any', '.*');
});

Route::get('campaign/{id}/offer/page', 'HomeCtrl@campaign_landing');

Route::prefix('')->namespace('Pwa')->middleware([])->group(function () {
    Route::get('/{any?}', 'FrontEndCtrl@home')->name('home')->where('any', '.*');
});
